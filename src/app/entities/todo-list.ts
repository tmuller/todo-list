

import {TodoTask} from "../values/todo-task";

export class TodoList {
    id: number;
    name: string;
    taskList: TodoTask[];

    constructor(listName: string, id?: number) {
        this.id = id || null;
        this.name = listName;
        this.taskList = [];
    }

    addTask(task: TodoTask): TodoList {
        this.taskList.push(task);
        return this;
    }
}
