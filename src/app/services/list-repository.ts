
import {TodoList} from '../entities/todo-list';
import {Injectable} from "@angular/core";

@Injectable()
export class ListRepository {

    listById(listId: number): Promise<TodoList> {
        const returnVal = JSON.parse(localStorage.getItem('list-' + listId));
        return Promise.resolve(returnVal);
    }

    list(): Promise<TodoList[]> {
        const idList: number[] = JSON.parse(localStorage.getItem('idList')) || [];
        const lists: TodoList[] = [];
        const numLists = idList.length;

        for (let i = 0; i < numLists; i++) {
            // Not ideal. Calling this method from an Rx Operators
            // coerces `this` to an Rx Object. Trying .bind() on it
            // isn't allowed by Typescript, so as a stop-gap, duplicating
            // the code from listById
            const returnVal = JSON.parse(localStorage.getItem('list-' + idList[i]));
            lists.push(returnVal);
        }

        return Promise.resolve(lists);
    }

    save(list: TodoList): Promise<TodoList> {

        const listIds = JSON.parse(localStorage.getItem('idList')) || [];
        if (list.id === null) {
            list.id = Date.now();
            listIds.push(list.id);
        }

        localStorage.setItem('idList', JSON.stringify(listIds));
        localStorage.setItem('list-' + list.id, JSON.stringify(list));

        return Promise.resolve(list);
    }



}
