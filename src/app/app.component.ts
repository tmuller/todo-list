import {Component} from '@angular/core';
import {NotificationService} from './components/ui-notifications/notification-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [NotificationService]
})

export class AppComponent {

  /**
   * The current notification to be displayed. Currently allowing only
   * one notification, which will be passed to the notification component
   */
  notification: Notification = null;

  constructor(private noticeService: NotificationService) {}
}
