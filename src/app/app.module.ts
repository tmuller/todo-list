import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module'

import { AppComponent } from './app.component';
import {TaskListEditComponent} from './components/listedit/listedit.component';
import {UiNotificationsComponent} from './components/ui-notifications/ui-notifications.component';
import {ListManagementComponent} from './components/list-management/list-management.component';
import {TaskManagementComponent} from './components/task-management/task-management.component';

@NgModule({
  declarations: [
    AppComponent,
    TaskListEditComponent,
    UiNotificationsComponent,
    TaskListEditComponent,
    ListManagementComponent,
    TaskManagementComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
