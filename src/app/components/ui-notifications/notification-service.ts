/**
 * Injectable service to display a global message from anywhere
 * in the system
 */

import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {UiNotification} from '../../values/notifications';

@Injectable()
export class NotificationService {

    public notices: Subject<UiNotification> = new Subject<UiNotification>();

    /**
     * Set up a curried method to display error messages. The first
     * variant takes a severity (for styling) and a message, the second
     * takes a severity, a function for processing and a piece of data on
     * which to operate.
     */
    displayUiMessage(severity: string, processor?: Function): (d: any) => void {
        return function(message: any) {

            const notification = {
                title: '',
                severity: severity,
                message: (processor && typeof processor == 'function') ? processor(message) : message
            };
            // console.log(notification);
            this.notices.next(notification);
        }.bind(this);
    }

}
