/**
 * UI notifications. Building my own for learning experience
 */

import {Component, Input, OnChanges, OnInit, SimpleChange} from '@angular/core';
import {UiNotification} from '../../values/notifications';
import {NotificationService} from './notification-service';

@Component({
    selector: 'ui-notifications',
    templateUrl: './ui-notifications.component.html'
})

export class UiNotificationsComponent implements OnInit {

    private notification: UiNotification = {title: '', severity: '', message: ''};

    constructor(private notificationService: NotificationService) {}

    ngOnInit(): void {
        this.notificationService.notices
            .subscribe((notification) => {this.notification = notification});
    }

}
