import {Component, OnInit} from '@angular/core';
import {Observable, ObservableInput} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {TodoList} from '../../entities/todo-list';
import {ListRepository} from '../../services/list-repository';
import {NotificationService} from '../ui-notifications/notification-service';



@Component({
    selector: 'todo-list',
    templateUrl: './list-management.component.html',
    styleUrls: ['./list-management.component.css'],
    providers: [ListRepository, NotificationService],
})

export class ListManagementComponent implements OnInit {


    private editListDetailResultObs: Observable<any> = new Subject();

    /**
     * A collection of all lists in the system
     */
    taskLists: TodoList[] = [];

    /**
     * An instance of the currently edited list
     */
    currentlyEditedList: TodoList = null;

    constructor(private listRepo: ListRepository, private notifications: NotificationService) {
    }

    ngOnInit(): void {
        this.listRepo.list().then((list) => this.taskLists = list );

        // Set up the processing of events from the child component handling
        // editing of lists, both error and success
        this.editListDetailResultObs
            .catch(this.onListUpdateError)           // handle an error from the edit list detail component
            .do((x) => { this.currentlyEditedList = null; })    // Reset the currently edited list to be empty
            .do((rec) => {
                this.notifications.displayUiMessage('success')('Updated List "' + rec.name + '"');
            })
            .flatMap(this.listRepo.list)             // Get the latest list from the repo to display
            .subscribe(
                (todoList) => { this.taskLists = todoList; },
                this.onListLoadError.bind(this)
            );

    };

    /**
     * Creates a new empty list as currentlyEditedList to open the edit dialog
     */
    onEditListClick(list?: TodoList): void {
        this.currentlyEditedList = (list) ? list : new TodoList('');
    };

    onListUpdateError(error, obs): ObservableInput<{}> {
        // TODO: Show an error message

        // On error, there is currently nothing we can do, so
        // return an empty Observable to keep the flow running
        return new Subject();
    }

    /**
     * Handles any errors caused by retrieving the todo lists from the
     * repository.
     */
    onListLoadError(err): void {
        this.notifications.displayUiMessage('error')('Could not load your Todo lists');
    }

    onViewListClick(list: TodoList): void {
        console.log(list);
    }

    numberOpen(list: TodoList): number {
        return list.taskList.filter( (t) => !t.completed).length;
    }

    fetchAllLists(k): Promise<TodoList[]> {
        console.log('fetchAllLists called', k, this);
        return this.listRepo.list();
    }

}

