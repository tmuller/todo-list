/**
 * Component dealing with the editing of task lists
 */

import {
    Component, EventEmitter, Input, OnChanges, OnInit, Output,
    SimpleChange
} from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import {TodoList} from '../../entities/todo-list';
import {Observable, Subject} from 'rxjs/Rx';
import {ListRepository} from '../../services/list-repository';
import {withLatestFrom} from 'rxjs/operator/withLatestFrom';

@Component({
    selector: 'tasklist-edit',
    templateUrl: './listedit.component.html',
    styleUrls: ['./listedit.component.css']
})

export class TaskListEditComponent implements OnInit, OnChanges {

    /**
     * Declare the input/output avenues for this component
     */
    @Input() taskList: TodoList;
    @Output() onListUpdated = new EventEmitter<TodoList>();

    private formSubmit: Observable<any> = new Subject();

    public editForm: FormGroup = new FormGroup({
        listname: new FormControl()
    });

    constructor(private listRepo: ListRepository) {}

    ngOnInit() {

        this.formSubmit
            .withLatestFrom(this.editForm.valueChanges, this.mergeListAndFormData)
            .do( () => { this.editForm.reset(); } )
            .flatMap(this.listRepo.save)
            .subscribe((updatedList) => {this.onListUpdated.emit(updatedList)});

    }

    ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
        if (changes.currentValue !== null && this.taskList) {
            this.editForm.patchValue({listname: this.taskList.name});
        }
    }

    onCancelEdit() {
        this.taskList = null;
        this.editForm.reset();
    }

    mergeListAndFormData(list, formValues): TodoList {
        list.name = formValues.listname;
        return list;
    }


}
