

import {Component, Input, OnInit} from '@angular/core';
import {TodoList} from '../../entities/todo-list';
import {ListRepository} from '../../services/list-repository';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {FormControl, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {TodoTask} from '../../values/todo-task';

@Component({
    selector: 'task-editor',
    templateUrl: 'task-management.component.html',
    providers: [ ListRepository ],
    styleUrls: ['./task-management.component.css']
})

export class TaskManagementComponent implements OnInit {

    @Input() activeList: TodoList;

    private formObservable: Observable<any> = new Subject();

    /**
     * Data model behind the form, a simple textarea to enter
     * new tasks, one per line
     */
    public addTasks: FormGroup = new FormGroup({
        newTasks: new FormControl('')
    });

    constructor(
        private listRepo: ListRepository,
        private activeRoute: ActivatedRoute,
        private router: Router
    ) {

    }


    ngOnInit(): void {

        // Subscribe to the Observable containing the URL parameters
        this.activeRoute.paramMap
            .flatMap( (para: ParamMap) => this.listRepo.listById(+para.get('id')))
            .subscribe(
                (l) => { this.activeList = l; },
                (err) => { console.error(err); }
            );

        // Set up the processing of form data into a new task list
        this.formObservable
            .withLatestFrom(this.addTasks.valueChanges, this.processFormInput)
            .do( (x) => { this.addTasks.reset(); } )
            .subscribe(
                (result) => { this.listRepo.save(result); },
                (err) => {console.error(err); }
            );

    }


    processFormInput(current: TodoList, formData): TodoList {

        if (formData.newTasks && typeof formData.newTasks == 'string') {
            const newTaskList = formData.newTasks.split("\n").reduce((acc, entry) => {
                if (entry) acc.push({description: entry, completed: false});
                return acc;
            }, current.taskList);
            current.taskList = newTaskList;
        }
        return current;
    }


    returnToList(): void {
        this.router.navigateByUrl('/')
    }

    onTaskCompletedClick(task: TodoTask): void {
        if (!task.completed) task.completed = true;
        this.listRepo.save(this.activeList);
    }


}
