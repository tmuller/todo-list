

export class UiNotification {
    severity: string;
    message: string;
    title: string;
}
