
export class TodoTask {
    description: string;
    completed: boolean;
}
