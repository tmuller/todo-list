import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListManagementComponent} from './components/list-management/list-management.component';
import {TaskManagementComponent} from './components/task-management/task-management.component';
import {TaskListEditComponent} from './components/listedit/listedit.component';

const routes: Routes = [
    { path: '', component:  ListManagementComponent},
    { path: 'list/:id', component: TaskListEditComponent},
    { path: 'tasks/:id', component: TaskManagementComponent}
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
